package com.ranull.doorbells;

import com.ranull.doorbells.bell.BellManager;
import com.ranull.doorbells.commands.DoorbellsCommand;
import com.ranull.doorbells.data.DataManager;
import com.ranull.doorbells.events.Events;
import org.bukkit.plugin.java.JavaPlugin;

public class Doorbells extends JavaPlugin {
	@Override
	public void onEnable() {
		DataManager dataManager = new DataManager(this);
		BellManager bellManager = new BellManager(this);

		dataManager.createDataFile();

		saveDefaultConfig();

		getCommand("doorbells").setExecutor(new DoorbellsCommand(this));

		getServer().getPluginManager().registerEvents(new Events(this, bellManager, dataManager), this);
	}
}