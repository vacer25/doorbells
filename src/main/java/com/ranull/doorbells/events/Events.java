package com.ranull.doorbells.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.data.type.NoteBlock;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.ranull.doorbells.Doorbells;
import com.ranull.doorbells.bell.BellManager;
import com.ranull.doorbells.data.DataManager;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Events implements Listener {
	private BellManager bellManager;
	private DataManager dataManager;
	private String placeMessage;
	private String removeMessage;
	private boolean cancel;
	private boolean shift;
	private boolean needNoteblock;

	public Events(Doorbells plugin, BellManager bellManager, DataManager dataManager) {
		this.bellManager = bellManager;
		this.dataManager = dataManager;
		this.placeMessage = plugin.getConfig().getString("settings.placeMessage").replace("&", "§");
		this.removeMessage = plugin.getConfig().getString("settings.removeMessage").replace("&", "§");
		this.cancel = plugin.getConfig().getBoolean("settings.cancel");
		this.shift = plugin.getConfig().getBoolean("settings.shift");
		this.needNoteblock = plugin.getConfig().getBoolean("settings.needNoteblock");
	}

	// Ring doorbell
	@EventHandler
	public void ringDoorbell(PlayerInteractEvent event) {
		if (!event.getPlayer().hasPermission("doorbells.use")) {
			return;
		}

		if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (dataManager.getMaterials().contains(event.getClickedBlock().getType().toString())) {
				Location location = event.getClickedBlock().getLocation();

				if(needNoteblock && event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.NOTE_BLOCK)) {

					if (!event.getPlayer().hasPermission("doorbells.place")) {
						return;
					}

					if (shift) {
						if (!event.getPlayer().isSneaking()) {
							if (!dataManager.checkDoorbell(location)) {
								return;
							}
						}
					}

					if (!dataManager.checkDoorbell(location)) {
						dataManager.saveDoorbell(location);
						if (!placeMessage.equals("")) {
							event.getPlayer().sendMessage(placeMessage);
							if (cancel) {
								event.setCancelled(true);
							}
							return;
						}
					}

				}

				if (dataManager.checkDoorbell(location)) {
					bellManager.ringDoorbell(location);
					if (cancel) {
						event.setCancelled(true);
					}
				}
			}
		}
	}

	// Place doorbell
	@EventHandler
	public void placeDoorbell(BlockPlaceEvent event) {
		if (needNoteblock || !event.getPlayer().hasPermission("doorbells.place")) {
			return;
		}

		if (shift) {
			if (!event.getPlayer().isSneaking()) {
				return;
			}
		}

		if (dataManager.getMaterials().contains(event.getBlock().getType().toString())) {
			dataManager.saveDoorbell(event.getBlock().getLocation());
			if (!placeMessage.equals("")) {
				event.getPlayer().sendMessage(placeMessage);
			}
		}
	}

	// Remove doorbell
	@EventHandler
	public void removeDoorbell(BlockBreakEvent event) {
		if (dataManager.getMaterials().contains(event.getBlock().getType().toString())) {
			if (dataManager.checkDoorbell(event.getBlock().getLocation())) {
				if (!event.getPlayer().hasPermission("doorbells.remove")) {
					event.setCancelled(true);
					return;
				}

				dataManager.removeDoorbell(event.getBlock().getLocation());
				if (!removeMessage.equals("")) {
					event.getPlayer().sendMessage(removeMessage);
				}
			}
		}
	}
}
