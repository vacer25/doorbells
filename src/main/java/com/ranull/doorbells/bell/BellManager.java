package com.ranull.doorbells.bell;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;

import com.ranull.doorbells.Doorbells;

public class BellManager {
	private Doorbells plugin;

	public BellManager(Doorbells plugin) {
		this.plugin = plugin;
	}

	public void ringDoorbell(Location loc) {
		int delay = plugin.getConfig().getInt("settings.delay");
		Boolean first = plugin.getConfig().getBoolean("settings.first");
		Sound firstSound = Sound.valueOf(plugin.getConfig().getString("settings.firstSound"));
		Float firstVolume = Float.valueOf(plugin.getConfig().getString("settings.firstVolume"));
		Float firstPitch = Float.valueOf(plugin.getConfig().getString("settings.firstPitch"));
		Boolean second = plugin.getConfig().getBoolean("settings.second");
		Sound secondSound = Sound.valueOf(plugin.getConfig().getString("settings.secondSound"));
		Float secondVolume = Float.valueOf(plugin.getConfig().getString("settings.secondVolume"));
		Float secondPitch = Float.valueOf(plugin.getConfig().getString("settings.secondPitch"));

		// First
		if (first) {
			loc.getWorld().playSound(loc, firstSound, firstVolume, firstPitch);
		}
		// Second
		if (second) {
			new BukkitRunnable() {
				@Override
				public void run() {
					loc.getWorld().playSound(loc, secondSound, secondVolume, secondPitch);
				}
			}.runTaskLater(plugin, delay);
		}
	}
}
